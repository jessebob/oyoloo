﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Xml.Linq;
using System.Globalization;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Discount_city.Models;

namespace Discount_city.Controllers
{
    public class ProductController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Product
        public ActionResult Index()
        {
            return View(db.Product.ToList());
        }

        // GET: Product/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Product.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id2,Id,EAN,Title,Brand,Shortdescription,Fulldescription,Image,Weight,Price,Category,Subcategory,Subsubcategory")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Product.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(product);
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Product.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id2,Id,EAN,Title,Brand,Shortdescription,Fulldescription,Image,Weight,Price,Category,Subcategory,Subsubcategory")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(product);
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Product.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Product.Find(id);
            db.Product.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Category/Import
        public ActionResult Import()
        {
            try
            {
                //Any records present?
                if (db.Product.Any())
                {
                    return RedirectToAction("Index");
                }

                var el = XElement.Load("https://supermaco.starwave.nl/api/products");
                var products = el.Elements();

                // Add categories
                foreach (var product in products)
                {
                    db.Product.Add(new Product
                    {
                        Id = short.Parse(product.Attribute("Id").Value),
                        EAN = long.Parse(product.Element("EAN").Value),
                        Title = product.Element("Title").Value,
                        Brand = product.Element("Brand").Value,
                        Shortdescription = product.Element("Shortdescription").Value,
                        Fulldescription = product.Element("Fulldescription").Value,
                        Image = product.Element("Image").Value,
                        Weight = product.Element("Weight").Value,
                        Price = decimal.Parse(product.Element("Price").Value, CultureInfo.GetCultureInfo("en-US")),
                        Category = product.Element("Category").Value,
                        Subcategory = product.Element("Subcategory").Value,
                        Subsubcategory = product.Element("Subsubcategory").Value
                    });
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
