﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Xml.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Discount_city.Models;

namespace Discount_city.Controllers
{
    public class CategoryController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Category
        public ActionResult Index()
        {
            return View(db.Category.ToList());
        }

        // GET: Category/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Category.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Category/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] Category category)
        {
            if (ModelState.IsValid)
            {
                db.Category.Add(category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(category);
        }

        // GET: Category/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Category.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Category/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] Category category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        // GET: Category/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Category.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Category category = db.Category.Find(id);
            db.Category.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Category/Import
        public ActionResult Import()
        {
            try
            {
                //Any records present?
                if (db.Category.Any())
                {
                    return RedirectToAction("Index");
                }

                var el = XElement.Load("https://supermaco.starwave.nl/api/categories");
                var categories = el.Elements();

                // Add categories
                foreach (var category in categories)
                {
                    db.Category.Add(new Category
                    {
                        Name = category.Element("Name").Value
                    });
                }

                db.SaveChanges();

                // Add subcategories
                foreach (var category in categories)
                {
                    foreach (var subcategory in category.Elements("Subcategory"))
                    {
                        db.SubCategory.Add(new SubCategory
                        {
                            CategoryName = category.Element("Name").Value,
                            Name = subcategory.Element("Name").Value
                        });
                    }
                }

                db.SaveChanges();

                // Add subsubcategories
                foreach (var category in categories)
                {
                    foreach (var subcategory in category.Elements("Subcategory"))
                    {
                        foreach (var subsubcategory in subcategory.Elements("Subsubcategory"))
                        {
                            db.SubSubCategory.Add(new SubSubCategory
                            {
                                CategoryName = category.Element("Name").Value,
                                SubCategoryName = subcategory.Element("Name").Value,
                                Name = subsubcategory.Element("Name").Value
                            });
                        }
                    }
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
