﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Discount_city.Startup))]
namespace Discount_city
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
