﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Discount_city.Models
{
    public class SubCategory
    {
        [Key]
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
    }
}