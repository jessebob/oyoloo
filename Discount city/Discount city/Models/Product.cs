﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Discount_city.Models
{
    public class Product
    {
        [Key] 
        public int Id2 { get; set; }
        public short Id { get; set; }
        public long EAN { get; set; }
        public string Title { get; set; }
        public string Brand { get; set; }
        public string Shortdescription { get; set; }
        public string Fulldescription { get; set; }
        public string Image { get; set; }
        public string Weight { get; set; }
        public decimal Price { get; set; }
        public string Category { get; set; }
        public string Subcategory { get; set; }
        public string Subsubcategory { get; set; }
    }
}